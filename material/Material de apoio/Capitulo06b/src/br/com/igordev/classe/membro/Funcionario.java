/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.igordev.classe.membro;

import static br.com.igordev.util.Formata.data;
import static br.com.igordev.util.Formata.moeda;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author igordev
 */
public class Funcionario {
    
    private String nome;
    private double salario;
    private List<Periodo> historico = new ArrayList<>();
    
    
    public Funcionario(String nome, double salario) {
        this.nome = nome;
        this.salario = salario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
    
    public void adicionaPeriodo(String departamento, LocalDate dataEntrada, LocalDate dataSaida) {
        this.historico.add(new Periodo(departamento, dataEntrada, dataSaida));
    }

    private class Periodo {
        private String departamento;
        private LocalDate dataEntrada;
        private LocalDate dataSaida;

        public Periodo(String departamento, LocalDate dataEntrada, LocalDate dataSaida) {
            this.departamento = departamento;
            this.dataEntrada = dataEntrada;
            this.dataSaida = dataSaida;
        }       
        

        @Override
        public String toString() {
            String info = "\tDepartamento: " + departamento;
            info += "\n\tPeriodo: ";
            info += data(dataEntrada) + " até " + data(dataSaida);
            return info;
        }
    }

    @Override
    public String toString() {
        String info =  "Funcionario: " + nome;
        info += "\nSalário: " + moeda(salario);
        for (Periodo p : historico) {
            info += "\n" + p + "\n";
        }
        return info;
    }
   
}

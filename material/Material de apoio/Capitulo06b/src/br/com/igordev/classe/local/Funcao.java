/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.igordev.classe.local;

/**
 *
 * @author igordev
 */
public class Funcao {
    
    public int soma(int v1, int v2) {
        class Somador {
            public int executa(int v1, int v2) {
                return v1 + v2;
            }
        }
        return new Somador().executa(v1, v2);
    }
    
}

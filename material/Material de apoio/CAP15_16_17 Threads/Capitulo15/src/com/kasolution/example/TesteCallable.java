/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kasolution.example;

import java.text.NumberFormat;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author 130955800299
 */
public class TesteCallable {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Callable<Double> c = () -> {
            Double soma = 0.0;
            for (int i = 0; i <= 1000; i++) {
                soma += i;
                Thread.sleep(500);
            }
            return soma;
        };

        ExecutorService es = Executors.newFixedThreadPool(2);
        Future<Double> f1 = es.submit(c);
        Future<Double> f2 = es.submit(c);
        es.shutdown();

//        try {
//            System.out.printf("Soma: %1.2f\n", +f1.get(10, TimeUnit.NANOSECONDS));
//            System.out.printf("Soma: %1.2f\n", +f2.get(10, TimeUnit.NANOSECONDS));
//        } catch (TimeoutException ex) {
//            System.out.println("Tempo de Espera esgotado!");
//        }
        
        System.out.printf("Soma: %1.2f\n", +f1.get());
        System.out.printf("Soma: %1.2f\n", +f2.get());

    }
}

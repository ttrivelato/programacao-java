/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collectorsteste;

import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author 130955800299
 */
public class CollectorsTeste {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("=== MODO ANALITICO EX.1 ===");
        Stream<String> stream1 = Stream.of("w", "o", "l", "f");
        //Collector<Container do resultado, Acumulador de objetos, Jogar os objetos acumulados no Container>
        //String s1 = stream1.collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString();
        String s1 = stream1.collect(() -> new StringBuilder(), (sb1, s) -> sb1.append(s), (sb1, sb2) -> sb1.append(sb2)).toString();
        System.out.println(s1);

        System.out.println("=== MODO SINTÉTICO EX.1===");
        stream1 = Stream.of("w", "o", "l", "f");
        String s2 = stream1.collect(Collectors.joining(""));
        System.out.println(s2);

        System.out.println("=== MODO ANALITICO EX.2 ===");
        Stream<String> stream2 = Stream.of("w","o","l","f");
        //Set set = stream2.collect(TreeSet::new, TreeSet::add, TreeSet::addAll);
        Set set = stream2.collect(() -> new TreeSet(), (ts, e) -> ts.add(e), (ts1, ts2) -> ts1.addAll(ts2));
        set.forEach(System.out::print);
        System.out.println();
        System.out.println("=== MODO SINTÉTICO EX.2 ===");
        stream2 = Stream.of("w","o","l","f");
        Set set2 = stream2.collect(Collectors.toCollection(() -> new TreeSet()));
        //Set set2 = stream2.collect(Collectors.toCollection(TreeSet::new));
        //Set set2 = stream2.collect(Collectors.toSet());
        set2.forEach(System.out::print);
        System.out.println();

        System.out.println("=== MODO ANALITICO COM TESTE ANTES DE ADD ===");
        stream2 = Stream.of("w", "o", "l", "f");
        Set set3 = stream2.collect(() -> new TreeSet(), (ts, e) -> {
            if (e.equals("o") || e.equals("l")) {
                ts.add(e);
            }
        }, (ts1, ts2) -> ts1.addAll(ts2));
        set3.forEach(System.out::print);
        System.out.println();



    }

}

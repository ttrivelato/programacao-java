/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kasoluton.teste;

import br.com.kasoluton.dominio.dados.Funcionario;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author igordev
 */
public class ExemploComparator {

    /**
     * @param args the command line arguments
     */
    
    private static List<Funcionario> funcionarios = new ArrayList<>();
    
    public static void carregaLista() {
        funcionarios.addAll(Funcionario.getFuncionarios());
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        carregaLista();
        System.out.println("Ordenado por nome: ");
        Collections.sort(funcionarios, (f1, f2) -> f1.getNome().compareTo(f2.getNome()));
        funcionarios.forEach(System.out::println);
        System.out.println("\nOrdenado por idade:");
        Collections.sort(funcionarios, Funcionario::ordenaIdade);
        funcionarios.forEach(System.out::println);
    }
}

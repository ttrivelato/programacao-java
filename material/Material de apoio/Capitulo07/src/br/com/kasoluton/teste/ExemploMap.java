/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kasoluton.teste;

import br.com.kasoluton.dominio.dados.Funcionario;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author igordev
 */
public class ExemploMap {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        List<Funcionario> funcionarios = Funcionario.getFuncionarios();
        Map<Integer, Funcionario> map = new HashMap<>();
        //adiciona funcionários ao mapa
        funcionarios.forEach(f -> addFuncionario(map, f));
        //pegar o keySet do mapa
        Set<Integer> keys = map.keySet();
        keys.forEach(i -> System.out.println("[" + String.format("%03d", i) + "] - " + map.get(i)));
       
    }
    
    public static void addFuncionario(Map<Integer, Funcionario> map, Funcionario f) {
        map.put(f.getCodigo(), f);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kasoluton.teste;

import br.com.kasoluton.dominio.dados.Funcionario;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author igordev
 */
public class ExemploSet {
    
    private static Set<Funcionario> funcionarios = new TreeSet<>();
    
    public static void carrega() {
        ExemploSet.funcionarios.addAll(Funcionario.getFuncionarios());
    }
    
    public static void main(String[] args) {
        carrega();
        funcionarios.forEach(System.out::println);
    }
    
}

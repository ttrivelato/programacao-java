
import java.time.LocalDateTime;
import static java.time.Month.*;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author igort
 */
public class Exemplo01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ZoneId SaoPaulo = ZoneId.of("America/Sao_Paulo");
        ZoneId London = ZoneId.of("Europe/London");
        LocalDateTime meeting = LocalDateTime.of(2018, SEPTEMBER, 20, 15, 30);
        ZonedDateTime staffCall = ZonedDateTime.of(meeting, SaoPaulo);
        ZonedDateTime staffCallLondon = staffCall.withZoneSameInstant(London); 
        System.out.println("Staff call (Brazil) is at: " + staffCall);
        System.out.println("Staff call (UK) is at:     " + staffCallLondon);
    }

}

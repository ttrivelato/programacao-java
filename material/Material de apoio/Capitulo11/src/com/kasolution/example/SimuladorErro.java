/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kasolution.example;

import java.io.IOException;

/**
 *
 * @author igort
 */
public class SimuladorErro {
    
    public void erro01() throws KaException {
        throw new KaException("Simulação erro01!");
    }
    
    public void erro02() throws KaSysException {
        throw new KaSysException("Simulação erro02!");
    }
    
    public void erro03() throws IOException {
        throw new IOException("Simulação de erro03!");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kasolution.example;

/**
 *
 * @author igort
 */
public class KaSysException extends KaException {

    public KaSysException() {
    }

    public KaSysException(String string) {
        super(string);
    }

    public KaSysException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public KaSysException(Throwable thrwbl) {
        super(thrwbl);
    }
}

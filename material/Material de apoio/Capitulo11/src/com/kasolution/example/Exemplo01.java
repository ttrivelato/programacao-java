/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kasolution.example;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author igort
 */
public class Exemplo01 extends SimuladorErro {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
    }

    @Override
    public void erro01() throws KaException { //throws KaSysException //throws RuntimeException //nada //mas nunca throws Exception
        try {
            super.erro01(); //To change body of generated methods, choose Tools | Templates.
        } catch (KaException ex) {
            System.out.println("Erro: " + ex.getMessage());
        }
    }
    
    @Override
    public void erro02() throws RuntimeException {
        try {
            throw new KaSysException("Simulação erro01!");
        } catch (KaSysException ex) {
            System.out.println("Erro: " + ex.getMessage());
        }
    } 
}

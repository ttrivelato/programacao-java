/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kasolution.example;

import java.io.IOException;

/**
 *
 * @author igort
 */
public class Exemplo02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        SimuladorErro se = new SimuladorErro();
        try {
            se.erro02();
            se.erro03();
        } catch (KaSysException | IOException ex) {
            System.out.println("Erro: " + ex.getMessage());
        }
        
    }
    
}

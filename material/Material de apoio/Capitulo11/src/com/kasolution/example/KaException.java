/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kasolution.example;

import java.io.IOException;

/**
 *
 * @author igort
 */
public class KaException extends Exception {

    public KaException() {
    }

    public KaException(String string) {
        super(string);
    }

    public KaException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public KaException(Throwable thrwbl) {
        super(thrwbl);
    }
    
}

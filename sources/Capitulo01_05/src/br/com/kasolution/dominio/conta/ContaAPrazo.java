package br.com.kasolution.dominio.conta;

import br.com.kasolution.dominio.enums.Prazo;
import static br.com.kasolution.util.Formata.*;
import java.time.LocalDate;

public class ContaAPrazo {
    
    private String numero;
    private LocalDate dataMaturidade;
    private double saldo;
   
    public ContaAPrazo(String numero, Prazo prazo) {
        this.numero = numero;
        setDataMaturidade(prazo);    
    }
    
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public LocalDate getDataMaturidade() {
        return dataMaturidade;
    }

    public void setDataMaturidade(LocalDate dataMaturidade) {
        this.dataMaturidade = dataMaturidade;
    }
    
    public void setDataMaturidade(Prazo prazo) {
        System.out.println("Meses Maturidade: "+prazo);
        LocalDate hoje = LocalDate.now();
        
        this.dataMaturidade = hoje.plusMonths(prazo.getPrazo());
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    
    public void deposita(double valor) {
        if (valor < 1) {
            System.out.println("Deposito de "+moeda(valor)+" invalido. Valor não pode ser menor que 1 real");
        } else {
            this.saldo += valor;
            System.out.println("Deposito de "+moeda(valor)+" realizado com sucesso");
        }
    }
    
    public void saca(double valor) {
        LocalDate hoje = LocalDate.now();
        if (hoje.isAfter(dataMaturidade) || dataMaturidade.equals(hoje)) {
            if (getSaldo() >= valor) {
                setSaldo(getSaldo() - valor);
                System.out.println("Saque de "+moeda(valor)+" realizado com sucesso");
            } else {
                System.out.println("Saldo insuficiente.");
            }
        } else {
            System.out.println("Não atingiu a data de maturidade");
        }
    }
    
    @Override
    public String toString() {        
        String info = "\n \t Conta: "+ numero;
            info+= "\n \t Saldo: "+moeda(saldo);
            info+= "\n \t Data Maturidade: "+data(dataMaturidade);
            
        return info;        
    }
}
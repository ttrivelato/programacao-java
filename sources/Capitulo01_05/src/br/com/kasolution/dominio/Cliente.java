package br.com.kasolution.dominio;

import br.com.kasolution.dominio.conta.ContaAPrazo;

public class Cliente {
    
    private static int ultimoCodigo = 0;
    
    private int codigo;
    private String nome;
    private String cpf;
    private ContaAPrazo[] contasAPrazo;
    private int qtdContas;
    
    public Cliente() {
        this.codigo = ++ultimoCodigo;
        this.contasAPrazo = new ContaAPrazo[5];
    }
    
    public Cliente(String nome) {
        this();
        this.nome = nome;
    } 
    
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public void adicionaConta(ContaAPrazo contasAPrazo) {
        if (qtdContas < 5) {
            this.contasAPrazo[qtdContas++] = contasAPrazo;
        } else {
            System.out.println("Limite de contas por cliente atingido.");
        }
    }
    
    public void adicionaConta(ContaAPrazo...contasAPrazo) {
        for (ContaAPrazo c : contasAPrazo) {
            adicionaConta(c);
        }
    }
    
    @Override
    public String toString() {
        String info = "Cliente: " + nome;
        info += "\nRelação de contas:";
        for (ContaAPrazo c : contasAPrazo) {
            if (c != null) {
                info += "\n" + c + "\n";
            }
        }
        return info;
    }
}
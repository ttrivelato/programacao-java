package br.com.kasolution.dominio.enums;

public enum Prazo {
    
    AGORA(0),
    SEIS_MESES(6),
    DOZE_MESES(12),
    DEZOITO_MESES(18);
    
    private int prazo;
   
    Prazo(int prazo) {
        this.prazo = prazo;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }
}

package br.com.kasolution.teste;

import br.com.kasolution.dominio.conta.ContaAPrazo;
import br.com.kasolution.dominio.enums.Prazo;

public class TesteContaAPrazo02 {

    public static void main(String[] args) {
        ContaAPrazo cap1 = new ContaAPrazo("12345-5", Prazo.SEIS_MESES);       
        String imprime = cap1.toString();
        
        System.out.println("imprime "+imprime);

    }
}

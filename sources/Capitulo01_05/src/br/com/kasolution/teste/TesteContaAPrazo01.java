package br.com.kasolution.teste;

import br.com.kasolution.dominio.conta.ContaAPrazo;
import br.com.kasolution.dominio.enums.Prazo;
import br.com.kasolution.util.Formata;

public class TesteContaAPrazo01 {

    public static void main(String[] args) {
        ContaAPrazo cap1 = new ContaAPrazo("12345-6", Prazo.AGORA);
                
        cap1.deposita(500);
        cap1.deposita(1500);
        cap1.deposita(-500);
        
        System.out.println("Saldo "+Formata.moeda(cap1.getSaldo()));
        
        cap1.saca(100);
        
        System.out.println("Saldo "+Formata.moeda(cap1.getSaldo()));
        
        String imprime = cap1.toString();
        
        System.out.println("imprime "+imprime);

    }
}

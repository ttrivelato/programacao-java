package br.com.kasolution.teste;

import br.com.kasolution.dominio.Cliente;
import br.com.kasolution.dominio.conta.ContaAPrazo;
import br.com.kasolution.dominio.enums.Prazo;

public class TesteCliente {
    public static void main(String[] args) {
        ContaAPrazo cap1 = new ContaAPrazo("12345-6", Prazo.SEIS_MESES);
        ContaAPrazo cap2 = new ContaAPrazo("12345-7", Prazo.DOZE_MESES);
        ContaAPrazo cap3 = new ContaAPrazo("54321-0", Prazo.AGORA);
        ContaAPrazo cap4 = new ContaAPrazo("54321-1", Prazo.DEZOITO_MESES);
        ContaAPrazo cap5 = new ContaAPrazo("54321-8", Prazo.DEZOITO_MESES);
        
        Cliente c1 = new Cliente("Victor Augusto");
        c1.adicionaConta(cap1, cap2, cap3, cap4, cap5);
        
        System.out.println(c1);        
    }
}
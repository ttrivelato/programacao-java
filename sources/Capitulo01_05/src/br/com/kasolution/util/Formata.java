package br.com.kasolution.util;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Formata {
    public static String moeda(double valor) {
        return NumberFormat.getCurrencyInstance().format(valor);
    }
    
    public static String data(LocalDate data) {
        DateTimeFormatter f = DateTimeFormatter.ofPattern("dd/MM/yyyy - EEEE");
        return data.format(f);
    }
}
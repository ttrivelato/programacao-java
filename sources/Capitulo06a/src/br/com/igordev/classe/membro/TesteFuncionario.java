/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.igordev.classe.membro;

import java.time.LocalDate;

/**
 *
 * @author igordev
 */
public class TesteFuncionario {

    public static void main(String[] args) {
        Funcionario f = new Funcionario("Igor", 1000.00);
        f.adicionaPeriodo("Banco de Dados", LocalDate.of(2017, 2, 1), LocalDate.of(2017, 4, 30));
        f.adicionaPeriodo("Rede", LocalDate.of(2017, 5, 1), LocalDate.of(2018, 12, 31));
        f.adicionaPeriodo("Desenvolvimento", LocalDate.of(2019, 1, 1), LocalDate.now());
        System.out.println(f);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.igordev.classe.anonima;

import java.math.BigDecimal;

/**
 *
 * @author igordev
 */
public class Adicao implements Operacao {

    @Override
    public BigDecimal efetua(BigDecimal valorA, BigDecimal valorB) {
        return valorA.add(valorB);
    }
    
}

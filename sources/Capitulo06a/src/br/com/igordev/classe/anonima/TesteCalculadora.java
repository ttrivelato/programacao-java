/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.igordev.classe.anonima;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author igordev
 */
public class TesteCalculadora {

    public static void main(String[] args) {
        Calculadora c = new Calculadora();

        BigDecimal v1 = new BigDecimal(50.0);
        BigDecimal v2 = new BigDecimal(60.0);

        //Classe externa
        BigDecimal rAdicao = c.calcula(v1, v2, new Adicao());

        //Classe interna anônima
        BigDecimal rSubtracao = c.calcula(v1, v2, new Operacao() {
            @Override
            public BigDecimal efetua(BigDecimal valorA, BigDecimal valorB) {
                return valorA.subtract(valorB);
            }
        });
        
        //Lambda
        BigDecimal rDivisao = c.calcula(v1, v2, (valorA, valorB) -> valorA.divide(valorB, 2, RoundingMode.HALF_UP));
        
        //Metod reference
        BigDecimal rMultiplicacao = c.calcula(v1, v2, BigDecimal::multiply);
        
        
        System.out.println("Resultado Adição: " + rAdicao);
        System.out.println("Resuldado Subtração: " + rSubtracao);
        System.out.println("Resultado Multiplicação: " + rMultiplicacao);
        System.out.println("Resultado Divisão: " + rDivisao);
    }
}

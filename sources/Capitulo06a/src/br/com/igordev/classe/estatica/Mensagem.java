/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.igordev.classe.estatica;

/**
 *
 * @author igordev
 */
public class Mensagem {
    
    private static String texto;
    
    public static class Texto {
        
        public Texto(String texto) {
            Mensagem.texto = texto;
        }
        
        public void escreve() {
            System.out.println(Mensagem.texto);
        }
    }
    
}

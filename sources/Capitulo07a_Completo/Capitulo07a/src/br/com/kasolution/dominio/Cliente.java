package br.com.kasolution.dominio;

import static br.com.kasolution.util.Formata.data;
import java.time.LocalDate;
import java.time.Month;

public class Cliente implements Comparable<Cliente> {

    private int codigo;
    private String nome;
    private LocalDate dataNascimento;

    @Override
    public int compareTo(Cliente c) {
//        if (this.codigo > c.codigo) {
//            return 1;
//        } else if (this.codigo < c.codigo) {
//            return -1;
//        } else {
//            return 0;
//        }
        return c.nome.compareTo(this.nome);
    }

    public static class Builder {
        private int codigo = -1;
        private String nome = "-- sem nome --";
        private LocalDate dataNascimento = LocalDate.of(1800, 1, 1);
        
        public Cliente.Builder codigo(int codigo) {
            this.codigo = codigo;
            return this;
        }
        
        public Cliente.Builder nome(String nome) {
            this.nome = nome;
            return this;
        }
        
        public Cliente.Builder dataNascimento(LocalDate
                dataNascimento) {
            this.dataNascimento = dataNascimento;
            return this;
        }
        
        public Cliente build() {
            return new Cliente(this);
        }
    }//fim Builder
    
    private Cliente(Cliente.Builder builder) {
        this.codigo = builder.codigo;
        this.nome = builder.nome;
        this.dataNascimento = builder.dataNascimento;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getNome() {
        return nome;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    @Override
    public String toString() {
        String info = "Cliente: #"+codigo;
        info += "\nNome: " + nome;
        info += "\nData Nascimento: " + data(dataNascimento);
        return info;
    }
    
    
    
}









package br.com.kasolution.teste;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class TesteSet {
    public static void main(String[] args) {
        Set<String> palavras = new HashSet<>();
        palavras.add("janeiro");
        palavras.add("fevereiro");
        palavras.add("janeiro");
        System.out.println(palavras);
        System.out.println("\nExcluindo janeiro");
        palavras.remove("janeiro");
        System.out.println(palavras);
        
        System.out.println("\nLista Ordenada:");
        SortedSet<Integer> numeros = new TreeSet<>();
        numeros.add(5);
        numeros.add(3);
        numeros.add(4);
        numeros.add(4);
        numeros.add(2);
        numeros.add(1);
        System.out.println(numeros);
    }
}










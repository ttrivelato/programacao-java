package br.com.kasolution.teste;

import br.com.kasolution.dominio.Cliente;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

public class TesteCliente01 {

    public static void main(String[] args) {
        Cliente c1 = new Cliente.Builder()
                .codigo(1)
                .nome("Renato")
                .dataNascimento(LocalDate.now())
                .build();
        Cliente c2 = new Cliente.Builder()
                .codigo(2)
                .nome("Marcelo")
                .dataNascimento(LocalDate.now().minusDays(1))
                .build();
        Cliente c3 = new Cliente.Builder()
                .codigo(3)
                .nome("Ana")
                .dataNascimento(LocalDate.now().minusDays(3))
                .build();
        Cliente c4 = new Cliente.Builder()
                .codigo(4)
                .nome("Carlos")
                .dataNascimento(LocalDate.now().minusDays(7))
                .build();

        SortedSet<Cliente> clientes = new TreeSet<>();
        clientes.addAll(Arrays.asList(c1,c2,c3,c4));
        System.out.println(clientes);
        
    }
}






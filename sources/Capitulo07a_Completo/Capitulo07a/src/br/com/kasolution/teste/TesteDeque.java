package br.com.kasolution.teste;

import java.util.ArrayDeque;
import java.util.Deque;

public class TesteDeque {

    public static void main(String[] args) {
        Deque<String> letras = new ArrayDeque<>();
        letras.add("A");//adiciona no fim
        letras.add("B");//adiciona no fim
        letras.push("C");//empilha
        letras.push("D");//empilha
        letras.add("E");//adiciona no fim
        System.out.println(letras);
    }
}

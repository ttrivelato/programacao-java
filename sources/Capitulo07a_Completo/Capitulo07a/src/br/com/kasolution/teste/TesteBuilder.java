package br.com.kasolution.teste;

import br.com.kasolution.dominio.Cliente;
import java.time.LocalDate;
import java.time.Month;

public class TesteBuilder {

    public static void main(String[] args) {
        //só tenho o código
        Cliente c01 = new Cliente.Builder()
                .codigo(1).build();
        System.out.println(c01);
        System.out.println("\n");

        //tenho o nome e a data de nascimento
        Cliente c02 = new Cliente.Builder()
                .nome("Henrique Moreira")
                .dataNascimento(LocalDate.of(1975, 5, 22)).build();
        System.out.println(c02);        
        
    }
    
    
    
    
    
}

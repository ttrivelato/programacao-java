package br.com.kasolution.teste;

import java.util.ArrayList;
import java.util.List;

public class TesteArrayList {

    public static void main(String[] args) {
        List<String> palavras = new ArrayList<>();
        palavras.add("janeiro");
        palavras.add("fevereiro");
        palavras.add("janeiro");
        //palavras.add(1); erro de compilação
        System.out.println(palavras);

        System.out.println("\n Excluindo janeiro:");
        palavras.remove("janeiro");
        System.out.println(palavras);
        
        System.out.println("\nPrimeiro elemento:");
        System.out.println(palavras.get(0));
        
        System.out.println("\nTamanho da lista: ");
        System.out.println(palavras.size());
    }
}

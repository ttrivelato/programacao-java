package br.com.kasolution.dominio;

import static br.com.kasolution.util.Formata.data;
import java.time.LocalDate;

public class Cliente {
    
    private int codigo;
    private String name;
    private LocalDate dataNascimento;
    
    public static class Builder {        
        private int codigo;
        private String name;
        private LocalDate dataNascimento;
        
        public Cliente.Builder codigo(int codigo) {
            this.codigo = codigo;
            return this;
        }
        
        public Cliente.Builder name(String name) {
            this.name = name;
            return this;
        }
        
        public Cliente.Builder dataNascimento(LocalDate dataNascimento) {
            this.dataNascimento = dataNascimento;
            return this;
        }
        
        public Cliente build() {
            return new Cliente(this);
        }
    }
    
    private Cliente(Cliente.Builder builder) {
        this.codigo = builder.codigo;
        this.name = builder.name;
        this.dataNascimento = builder.dataNascimento;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getName() {
        return name;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    @Override
    public String toString() {
        String info = "Cliente: #" +codigo;
        info+= "\nNome: "+ name;
        info+= "\nNascimento: "+ data(dataNascimento);
        
        return info;
    }
}
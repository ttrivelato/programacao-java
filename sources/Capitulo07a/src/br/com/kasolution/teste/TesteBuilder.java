package br.com.kasolution.teste;

import br.com.kasolution.dominio.Cliente;
import java.time.LocalDate;
import java.time.Month;

public class TesteBuilder {

    public static void main(String[] args) {
        
        Cliente c1 = new Cliente.Builder()
        .codigo(1)
        .name("Joao Cruz")
        .dataNascimento(LocalDate.of(1975,5,22))
        .build();
        
        System.out.println(c1.toString());
        System.out.println("\n");
        
    }
}
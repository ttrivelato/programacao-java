/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kasolution.util;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author igordev
 */
public class Formata {
    public static String moeda(double valor) {
        return NumberFormat.getNumberInstance().format(valor);
    }
    
    public static String data(LocalDate data) {
        DateTimeFormatter f = DateTimeFormatter.ofPattern("dd/MM/yyyy - EEEE");
        return data.format(f);
    }
}

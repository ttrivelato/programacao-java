package br.com.kasolution.dominio.teste;

import br.com.kasolution.dominio.dados.Funcionario;
import java.text.NumberFormat;
import java.util.List;

public class Exemplo01 {

    public static void main(String[] args) {
        List<Funcionario> funcs = Funcionario.getFuncionarios();        
        
        //Pipeline
        System.out.println("..:::Lista de Mulheres:::..");
        funcs.stream()
                .filter(f -> f.getSexo() == 'F') //Filtro
                .peek(f -> System.out.print("\t")) // Antes do foreach posso fazer algo -> Metodo intermediario
                .forEach(f -> System.out.println(f));  //Foreach  
        
        
        //--------------------------------
        //NAO RECOMENTADO - Stream é utilizado para remover da memoria após da utilização
        /*
        List<Funcionario> mulheres = new ArrayList<>();
        funcs.stream()
                .filter(f -> f.getSexo() == 'F') //Filtro
                .forEach(f -> mulheres.add(f));  //Foreach  
        */
        //--------------------------------
        
        //Pipeline
        System.out.println("..:::Mulheres Nome:::..");
        funcs.stream()
                .filter(f -> f.getSexo() == 'F') //Filtro
                .map(Funcionario::getNome)
                .peek(nome -> System.out.print("\t")) // Antes do foreach posso fazer algo -> Metodo intermediario
                .forEach(System.out::println);  //Foreach
        
        
        //Pipeline
        System.out.println("..:::Media Salarial Homens:::..");
        double media = funcs.stream()
                .filter(f -> f.getSexo() == 'M') //Filtro
                .mapToDouble(f -> f.getSalario())
                .average().getAsDouble();
        System.out.println("Media Salarial Masculina: "+NumberFormat.getCurrencyInstance().format(media));
        
        //Pipeline
        System.out.println("..:::Media Salarial Feminina:::..");
        double mediaf = funcs.stream()
                .filter(f -> f.getSexo() == 'F') //Filtro
                .mapToDouble(f -> f.getSalario())
                .average().getAsDouble();
        System.out.println("Media Salarial Feminina: "+NumberFormat.getCurrencyInstance().format(mediaf));
        
    }
}
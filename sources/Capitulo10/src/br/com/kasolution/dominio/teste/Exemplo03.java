package br.com.kasolution.dominio.teste;

import br.com.kasolution.dominio.dados.Funcionario;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Exemplo03 {

    public static void main(String[] args) {
        List<Funcionario> funcs = Funcionario.getFuncionarios(); 
        System.out.println("..:::FUNCIONARIOS POR UF:::..");
        
        //Create MAP -> STRING // LIST
        Map<String, List<Funcionario>> funcionarios;
        
        //Execute filter
        funcionarios = funcs.stream()
                .collect(Collectors.groupingBy(f -> f.getEstado()));
        
        //Print in the screen
        funcionarios.forEach((uf,lista) -> {
            
            System.out.println("UF: "+uf);
            lista.stream()
                    .peek(f -> System.out.print("\t"))
                    .forEach(f ->  System.out.println(f));
            
            long total = lista.stream().count();
            System.out.println("Total: "+total);
            
        });     
    }
}
package br.com.kasolution.dominio.teste;

import br.com.kasolution.dominio.dados.Funcionario;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Exemplo05 {

    public static void main(String[] args) {
        List<Funcionario> funcs = Funcionario.getFuncionarios(); 
        System.out.println("..:::Nomes separados por virgua, analitico:::..");
        
        String s1 = funcs.stream()
                .collect(() -> new StringBuilder(),
                        (sb, f) -> sb.append(",").append(f.getNome()),
                        (sbAnt, sb) -> sbAnt.append(sb)
                ).substring(1);
        
        System.out.println("\t analitico: "+ s1);
        
        System.out.println("..:::Nomes separados por virgua, sintetico:::..");
        String s2 = funcs.stream()
                .map(f -> f.getNome())
                .collect(Collectors.joining(","));
        
        System.out.println("\t sintetico: "+ s2);
        
        //FORMA CORRETA - SET
        System.out.println("..:::Lista de Mulheres:::..");
        Set<Funcionario> set = funcs.stream()
                .filter(f -> f.getSexo() == 'F')
                .collect(Collectors.toCollection(() -> new TreeSet()));
                
        set.stream()
                .peek(f -> System.out.print("\t"))
                .forEach(System.out::println);
        
         //FORMA CORRETA - ARRAY LIST
        System.out.println("..:::Lista de Homens:::..");
        List<Funcionario> lista = funcs.stream()
                .filter(f -> f.getSexo() == 'M')
                .collect(Collectors.toCollection(() -> new ArrayList()));
                
        lista.stream()
                .peek(f -> System.out.print("\t"))
                .forEach(System.out::println);
    }
}
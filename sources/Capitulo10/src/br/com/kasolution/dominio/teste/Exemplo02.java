package br.com.kasolution.dominio.teste;

import br.com.kasolution.dominio.dados.Funcionario;
import java.util.List;
import java.util.Optional;

public class Exemplo02 {

    public static void main(String[] args) {
        List<Funcionario> funcs = Funcionario.getFuncionarios(); 
        
        //Pipeline
        System.out.println("..:::BUSCA PESSOA RIO JANEIRO:::..");
        Optional<Funcionario> funcionario = funcs.stream()
                .filter(f-> f.getEstado().equals("RJ"))
                .findFirst();
        
        //Verify exist data
        if(funcionario.isPresent()) {
            System.out.println("\t"+funcionario.get());
        }
        
        //Pipeline
        System.out.println("..:::VERIFICA, TODOS MORAM RIO JANEIRO?:::..");
        boolean b = funcs.stream()
                .allMatch(f-> f.getEstado().equals("RJ"));        
        
        //Verify is true - TODOS MORAM RIO JANEIRO
        if(b) {
            System.out.println("\t SIM - Todos Moram no RIO de Janeiro");
        } else {
            System.out.println("\t NAO - Tem pessoas que não moram no RIO de Janeiro");
        }        
    }
}
package br.com.kasolution.dominio.teste;

import br.com.kasolution.dominio.dados.Funcionario;
import java.util.List;
import java.util.Optional;

public class Exemplo04 {

    public static void main(String[] args) {
        List<Funcionario> funcs = Funcionario.getFuncionarios(); 
        System.out.println("..:::FUNCIONARIOS ORDENADO PELO NOME:::..");
        
        funcs.stream()
                .sorted((f1, f2) -> {
                    return sortName(f1, f2);
                })
                .peek(f -> System.out.print("\t"))
                .forEach(f ->  System.out.println(f));
        
        System.out.println("..:::FUNCIONARIOS ORDENADO POR IDADE:::..");        
        funcs.stream()
                .sorted((f1, f2) -> {
                    return sortIdade(f1, f2);
                })
                .peek(f -> System.out.print("\t"))
                .forEach(f ->  System.out.println(f));
        
        System.out.println("..:::FUNCIONARIOS MAIS VELHO:::..");        
        Optional<Funcionario> velho = funcs.stream()
                .max((f1,f2) -> sortIdade(f1, f2));
        
        if(velho.isPresent()) {
            System.out.println("Funcionario mais velho: "+velho.get()); 
        }
        
        System.out.println("..:::FUNCIONARIOS MAIS NOVO:::..");        
        Optional<Funcionario> novo = funcs.stream()
                .min((f1,f2) -> sortIdade(f1, f2));
        
        if(novo.isPresent()) {
            System.out.println("Funcionario mais novo: "+novo.get()); 
        }
        
    }
    
    public static int sortName(Funcionario f1, Funcionario f2) {        
        return f1.getNome().compareTo(f2.getNome());
    }
    
    public static int sortIdade(Funcionario f1, Funcionario f2) {   
        Integer i1 = f1.getIdade();
        Integer i2 = f2.getIdade();
        
        return i1.compareTo(i2);
    }
}
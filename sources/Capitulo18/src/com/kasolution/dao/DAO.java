package com.kasolution.dao;

import java.util.List;

public interface DAO<T> extends AutoCloseable {
    
    public void salva(T t) throws DAOException;
    public void atualizar(T t) throws DAOException;
    public boolean excluir(T t) throws DAOException;
    public T buscarId(int id) throws DAOException;
    public List<T> buscarTodos() throws DAOException;
    
    @Override
    public void close() throws DAOException;    
}
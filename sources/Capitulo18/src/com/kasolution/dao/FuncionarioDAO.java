package com.kasolution.dao;

import com.kasolution.domain.Funcionario;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FuncionarioDAO implements DAO<Funcionario> {
    
    private static final String SQL_INSERT = "INSERT INTO FUNCIONARIO (NOME, SALARIO)"
            + " VALUES (?,?)";
    private static final String SQL_UPDATE = "UPDATE FUNCIONARIO SET "
            + " NOME = ?, "
            + " SALARIO = ? "
            + " WHERE CODIGO = ? ";
    private static final String SQL_DELETE = "DELETE FROM FUNCIONARIO WHERE CODIGO = ?";
    private static final String SQL_QUERY_ALL = "SELECT CODIGO, NOME, SALARIO FROM FUNCIONARIO";
    private static final String SQL_QUERY_ONE = "SELECT CODIGO, NOME, SALARIO FROM FUNCIONARIO WHERE CODIGO = ?";
    
    private Connection conn;
    
    public FuncionarioDAO() {
        String url = "jdbc:mysql://localhost:3306/HR";
        String username = "root";
        String password = "";
        
        try {
            conn = DriverManager.getConnection(url, username, password);
        } catch(SQLException e) {
            System.out.println("SQLException "+e.getMessage());
        }
    }
    
    @Override
    public void salva(Funcionario f) throws DAOException {
        
        try(PreparedStatement stmt = conn.prepareStatement(SQL_INSERT)) {
            stmt.setString(1, f.getNome());
            stmt.setDouble(2, f.getSalario());
            
            if(stmt.executeUpdate() !=1) {
                throw new DAOException("Funcionario salva Erro a inserir: retorno -1");
            }
            
        } catch(SQLException e) {
            throw new DAOException("Erro ao inserir "+e.getMessage());
        }        
    }

    @Override
    public void atualizar(Funcionario f) throws DAOException {
        
        try(PreparedStatement stmt = conn.prepareStatement(SQL_UPDATE)) {
            stmt.setString(1, f.getNome());
            stmt.setDouble(2, f.getSalario());
            stmt.setInt(3, f.getCodigo());
            
            if(stmt.executeUpdate() !=1) {
                throw new DAOException("Funcionario atualizar Erro a atualizar: retorno -1");
            }
            
        } catch(SQLException e) {
            throw new DAOException("Erro ao atualizar "+e.getMessage());
        }
    }

    @Override
    public boolean excluir(Funcionario f) throws DAOException {
        try(PreparedStatement stmt = conn.prepareStatement(SQL_DELETE)) {
            stmt.setInt(1, f.getCodigo());
            
            return stmt.execute();
            
        } catch(SQLException e) {
            throw new DAOException("Erro ao excluir "+e.getMessage());
        }
    }

    @Override
    public Funcionario buscarId(int id) throws DAOException {
        Funcionario f = null;
        
        try(PreparedStatement stmt = conn.prepareStatement(SQL_QUERY_ONE)) {
            stmt.setInt(1, id);
            
            try(ResultSet rs = stmt.executeQuery()) {
                if(rs.next()) {
                    f = ormFuncionario(rs);
                }
            } catch(SQLException e) {
                throw new DAOException("Erro ResultSet ao buscarId "+e.getMessage());
            }
            
            return f;
            
        } catch(SQLException e) {
            throw new DAOException("Erro ao buscarId "+e.getMessage());
        }
    }

    @Override
    public List<Funcionario> buscarTodos() throws DAOException {
               
        List<Funcionario> funcs = new ArrayList<>();
        
        try(PreparedStatement stmt = conn.prepareStatement(SQL_QUERY_ALL)) {
                       
            try(ResultSet rs = stmt.executeQuery()) {
                while(rs.next()) {
                    funcs.add(ormFuncionario(rs));
                }
            } catch(SQLException e) {
                throw new DAOException("Erro ResultSet ao buscarTodos "+e.getMessage());
            }
            
            return funcs;
            
        } catch(SQLException e) {
            throw new DAOException("Erro ao buscarTodos "+e.getMessage());
        }
    }

    @Override
    public void close() throws DAOException {
        
        try{
            conn.close();
        } catch(SQLException e) {
            throw new DAOException("Erro fechando conexao: "+e.getMessage());
        }        
    }    

    private Funcionario ormFuncionario(ResultSet rs) throws SQLException {        
        return new Funcionario.Builder()
                .codigo(rs.getInt("CODIGO"))
                .nome(rs.getString("NOME"))
                .salario(rs.getDouble("SALARIO"))
                .build();
    }
}
package br.com.kasolution.externa;

import br.com.kasolution.funcional.Calculadora;
import java.math.BigDecimal;

public class TesteCalculadora {

    public static void main(String[] args) {
        
        Calculadora c = new Calculadora();

        BigDecimal v1 = new BigDecimal(150.0);
        BigDecimal v2 = new BigDecimal(50.0);

        //soma
        BigDecimal rAdicao = c.calcula(v1, v2, new Soma());       
        System.out.println("Resultado Adicao: " + rAdicao); 
        
        //subtrair
        BigDecimal rSubtracao = c.calcula(v1, v2, new Subtrai());       
        System.out.println("Resultado Subtracao: " + rSubtracao);
        
        //Multiplicar
        BigDecimal rMultiplicar = c.calcula(v1, v2, new Multiplicar());       
        System.out.println("Resultado rMultiplicar: " + rMultiplicar);
        
        //divisao
        BigDecimal rDivisao = c.calcula(v1, v2, new Divide());
        System.out.println("Resultado Divisao: " + rDivisao);
    }
}
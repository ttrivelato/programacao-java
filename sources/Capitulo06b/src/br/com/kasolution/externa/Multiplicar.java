/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kasolution.externa;

import br.com.kasolution.funcional.Operacao;
import java.math.BigDecimal;

class Multiplicar implements Operacao {

    @Override
    public BigDecimal efetua(BigDecimal v1, BigDecimal v2) {
        return v1.multiply(v2);
    }
}
package br.com.kasolution.funcional;

import java.math.BigDecimal;

@FunctionalInterface
public interface Operacao {    
    public BigDecimal efetua(BigDecimal valorA, BigDecimal valorB);    
}
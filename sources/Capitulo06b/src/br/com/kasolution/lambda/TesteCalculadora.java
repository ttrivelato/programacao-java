package br.com.kasolution.lambda;

import br.com.kasolution.funcional.Calculadora;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class TesteCalculadora {

    public static void main(String[] args) {
        
        Calculadora c = new Calculadora();

        BigDecimal v1 = new BigDecimal(150.0);
        BigDecimal v2 = new BigDecimal(50.0);
        
        //soma
        BigDecimal rSoma = c.calcula(v1, v2, (valorA, valorB) -> valorA.add(valorB));
        System.out.println("Resultado rSoma: " + rSoma);
        
        //subtrai
        BigDecimal rSubtrai = c.calcula(v1, v2, (valorA, valorB) -> valorA.subtract(valorB));
        System.out.println("Resultado rSubtrai: " + rSubtrai);
        
        //multiplica
        BigDecimal rMultiplica = c.calcula(v1, v2, (valorA, valorB) -> valorA.multiply(valorB));
        System.out.println("Resultado rMultiplica: " + rMultiplica);
        
        //divisao
        BigDecimal rDivisao = c.calcula(v1, v2, (valorA, valorB) -> valorA.divide(valorB, 2, RoundingMode.HALF_UP));
        System.out.println("Resultado rDivisao: " + rDivisao);
    }
}
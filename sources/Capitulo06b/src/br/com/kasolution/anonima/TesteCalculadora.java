package br.com.kasolution.anonima;

import br.com.kasolution.funcional.Calculadora;
import br.com.kasolution.funcional.Operacao;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class TesteCalculadora {

    public static void main(String[] args) {
        
        Calculadora c = new Calculadora();

        BigDecimal v1 = new BigDecimal(150.0);
        BigDecimal v2 = new BigDecimal(50.0);
        
        Operacao soma = new Operacao() {
            @Override
            public BigDecimal efetua(BigDecimal v1, BigDecimal v2) {
                return v1.add(v2);
            }
        };
        
        //soma
        BigDecimal rAdicao = c.calcula(v1, v2, soma);       
        System.out.println("Resultado Adicao: " + rAdicao); 
        
        Operacao subtrai = new Operacao() {
            @Override
            public BigDecimal efetua(BigDecimal v1, BigDecimal v2) {
                return v1.subtract(v2);
            }
        };
        
        //subtrai
        BigDecimal rSubtrai = c.calcula(v1, v2, subtrai);       
        System.out.println("Resultado subtrai: " + rSubtrai); 
        
        
        Operacao multiplica = new Operacao() {
            @Override
            public BigDecimal efetua(BigDecimal v1, BigDecimal v2) {
                return v1.multiply(v2);
            }
        };
        
        //multiplica
        BigDecimal rMultiplica = c.calcula(v1, v2, multiplica);       
        System.out.println("Resultado multiplica: " + rMultiplica);
        
        
        Operacao divide = new Operacao() {
            @Override
            public BigDecimal efetua(BigDecimal v1, BigDecimal v2) {
                return v1.divide(v2, 2, RoundingMode.HALF_UP);
            }
        };
        
        //multiplica
        BigDecimal rDivide = c.calcula(v1, v2, divide);       
        System.out.println("Resultado divide: " + rDivide);        
    }
}
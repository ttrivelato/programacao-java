package br.com.kasolution.interna;

import br.com.kasolution.funcional.Calculadora;
import br.com.kasolution.funcional.Operacao;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class TesteCalculadora {

    public static void main(String[] args) {        
        Calculadora c = new Calculadora();
        
        BigDecimal v1 = new BigDecimal(150.0);
        BigDecimal v2 = new BigDecimal(50.0);
        
        //rAdicao
        BigDecimal rAdicao = c.calcula(v1, v2, new TesteCalculadora() . new Soma());   
        System.out.println("Resultado rAdicao: " + rAdicao); 
        
        //rSubtrai
        BigDecimal rSubtrai = c.calcula(v1, v2, new TesteCalculadora() . new Subtrai());   
        System.out.println("Resultado rSubtrai: " + rSubtrai); 
        
        //rMutliplica
        BigDecimal rMutliplica = c.calcula(v1, v2, new TesteCalculadora() . new Mutliplica());   
        System.out.println("Resultado rMutliplica: " + rMutliplica); 
        
        //rMutliplica
        BigDecimal rDivide = c.calcula(v1, v2, new TesteCalculadora() . new Divide());   
        System.out.println("Resultado rDivide: " + rDivide);
    }
    
    private class Soma implements Operacao {

        @Override
        public BigDecimal efetua(BigDecimal v1, BigDecimal v2) {
            return v1.add(v2);
        }
    }
    
    private class Subtrai implements Operacao {

        @Override
        public BigDecimal efetua(BigDecimal v1, BigDecimal v2) {
            return v1.subtract(v2);
        }
    }
    
    private class Mutliplica implements Operacao {

        @Override
        public BigDecimal efetua(BigDecimal v1, BigDecimal v2) {
            return v1.multiply(v2);
        }
    }
    
    
    private class Divide implements Operacao {

        @Override
        public BigDecimal efetua(BigDecimal v1, BigDecimal v2) {
            return v1.divide(v2, 2, RoundingMode.HALF_UP);
        }
    }
}
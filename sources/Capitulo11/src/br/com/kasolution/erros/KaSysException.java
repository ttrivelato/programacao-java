package br.com.kasolution.erros;

public class KaSysException extends KaException{

    public KaSysException() {
    }

    public KaSysException(String string) {
        super(string);
    }

    public KaSysException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public KaSysException(Throwable thrwbl) {
        super(thrwbl);
    }
}
package br.com.kasolution.erros;

import java.io.IOException;

public class SimulaErro {
    
    public void erro01() throws KaException {
        System.out.println("ERRO 01");
        throw new KaException("Simulacao KaException ERRO 01");
    }
    
    public void erro02() throws KaSysException {
        System.out.println("ERRO 02");
        throw new KaSysException("Simulacao KaSysException ERRO 02");
    } 
    
    public void erro03() throws IOException {
        System.out.println("ERRO 03");
        throw new IOException("Simulacao IOException ERRO 03");
    } 
}

package br.com.kasolution.erros;

public class KaException extends Exception {

    public KaException() {
    }

    public KaException(String string) {
        super(string);
    }

    public KaException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public KaException(Throwable thrwbl) {
        super(thrwbl);
    }
}
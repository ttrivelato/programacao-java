package br.com.kasolution.erros;

import java.io.IOException;

public class Exemplo02 {
    
    public static void main(String[] args) {

        SimulaErro se = new SimulaErro();
        
        try {
            se.erro02();
            se.erro03();
        } catch(KaSysException | IOException e) {
            System.out.println("Erro: "+e.getMessage());
        } 
    }    
}
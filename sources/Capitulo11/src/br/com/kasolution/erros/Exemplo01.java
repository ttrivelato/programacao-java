package br.com.kasolution.erros;

public class Exemplo01 extends SimulaErro {

    public static void main(String[] args) throws KaException {
        Exemplo01 e = new Exemplo01();        
        e.erro01();
        e.erro02(); 
    }   
    
    //Pode ser suavizado 
    //KaException/KaSysException/RuntimeException/Nada
    @Override
    public void erro01() throws KaException {
        super.erro01();
    }
    
    @Override
    public void erro02() throws RuntimeException {
        try {
            throw new KaException("Erro 02");
        } catch (KaException ex) {
            System.out.println("Erro: "+ex.getLocalizedMessage());
        }
    }
}

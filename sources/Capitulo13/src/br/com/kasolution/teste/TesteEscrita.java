package br.com.kasolution.teste;

import br.com.kasolution.dominio.Carrinho;
import br.com.kasolution.dominio.Item;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TesteEscrita {

    public static void main(String[] args) {
        
        Carrinho carrinho = new Carrinho(1);
        
        carrinho.adiciona(
            new Item(200, "Monitor", 1500)
        );
        carrinho.adiciona(
            new Item(210, "Mouse", 49.90)
        );
        carrinho.adiciona(
            new Item(220, "Teclado", 89.90)
        );
        carrinho.adiciona(
            new Item(230, "Windows", 1089.90)
        );
        
        //Gerar arquivo
        String arquivo = "C:/temp/compra"+carrinho.getId()+".dat";
        
        try (
            FileOutputStream fos = new FileOutputStream(arquivo);
            ObjectOutputStream o = new ObjectOutputStream(fos)
            ) 
        {
            
            o.writeObject(carrinho);
            System.out.println("Gravacao OK");            
            
        } catch (FileNotFoundException ex) {
            System.out.println("ERRO FileNotFoundException "+ex.getMessage());
            Logger.getLogger(TesteEscrita.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.out.println("ERRO IOException "+ex.getMessage());
            Logger.getLogger(TesteEscrita.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }  
}
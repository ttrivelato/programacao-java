package br.com.kasolution.teste;

import br.com.kasolution.dominio.Carrinho;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TesteLeitura {

    public static void main(String[] args) {
        System.out.println("Conteudo do arquivo \n");
        String arquivo = "C:/temp/compra1.dat";
        
        Carrinho carrinho = null;
        
        try (
            FileInputStream fis = new FileInputStream(arquivo);
            ObjectInputStream o = new ObjectInputStream(fis)
            ) 
        {
            
            carrinho = (Carrinho) o.readObject();
            System.out.println(carrinho);
            
        } catch (FileNotFoundException ex) {
            System.out.println("ERRO FileNotFoundException "+ex.getMessage());
            Logger.getLogger(TesteEscrita.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.out.println("ERRO IOException "+ex.getMessage());
            Logger.getLogger(TesteEscrita.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            System.out.println("ERRO ClassNotFoundException "+ex.getMessage());
            Logger.getLogger(TesteLeitura.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
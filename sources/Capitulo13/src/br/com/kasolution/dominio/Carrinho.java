package br.com.kasolution.dominio;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Carrinho implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int id;
    private LocalDate dataCompra;
    private List<Item> itens;
    private transient double totalCompra;

    public Carrinho(int id) {
        this.id = id;
        this.itens = new ArrayList<>();
        this.dataCompra = LocalDate.now();
        this.totalCompra = 0;                
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDataCompra() {
        return dataCompra;
    }

    public void setDataCompra(LocalDate dataCompra) {
        this.dataCompra = dataCompra;
    }

    public List<Item> getItens() {
        return itens;
    }

    public void setItens(List<Item> itens) {
        this.itens = itens;
    }

    public double getTotalCompra() {
        return totalCompra;
    }

    public void setTotalCompra(double totalCompra) {
        this.totalCompra = totalCompra;
    }
    
    public void adiciona(Item item) {
        itens.add(item);
    }
    
    public void remove(Item item) {
        itens.remove(item);
    }
    
    @Override
    public String toString() {
        String info = "";
        info += "Carrinho #ID: "+ id + "\n";
        info += "[######LISTA DE ITENS######] \n";
        for(Item i :itens) {
            info += "Item ID: "+i.getId() +"\n";
            info += "Item Desc: "+i.getDescricao()+"\n";
            info += "Item R$: "+
                    NumberFormat.getCurrencyInstance().format(i.getValor())+"\n";
        }
        
        info += "Total da compra: "
                +NumberFormat.getCurrencyInstance().format(totalCompra)+"\n";
    
        return info;
    }
    
    private void writeObject(ObjectOutputStream oos) throws IOException {        
        oos.defaultWriteObject();  
        oos.writeObject(LocalDateTime.now());
    }
    
    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject();
        if(totalCompra == 0 & itens.size() > 0) {
            totalCompra = itens.stream()
                    .mapToDouble(i -> i.getValor()).sum();
        }
        LocalDateTime dataHoraArquivo = (LocalDateTime)
        ois.readObject();
        
        System.err.println("Data e hora geracao arquivo: "+ data(dataHoraArquivo));
    }
    
    private String data(LocalDateTime dataHora) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy - EEEE hh:mm:ss a");
        return dtf.format(dataHora);
   }
}
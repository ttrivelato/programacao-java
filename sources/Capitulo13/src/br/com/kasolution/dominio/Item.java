package br.com.kasolution.dominio;

import java.io.Serializable;

public class Item implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int id;
    private String descricao;
    private double valor;

    public Item(int id, String descricao, double valor) {
        this.id = id;
        this.descricao = descricao;
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}